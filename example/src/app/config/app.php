<?php

return [
    'env' => env('APP_ENV', 'production'),
    'debug' => env('APP_DEBUG', false),
    'url' => env('APP_URL', 'http://localhost'),

    'timezone'        => 'UTC',
    'locale'          => 'en',
    'fallback_locale' => 'en',
    'key'             => env('APP_KEY', 'SomeRandomString'),

    'cipher' => 'AES-256-CBC',

    'log' => 'single',

    'providers' => [
        Sebwite\Illuminate\Foundation\Providers\FoundationServiceProvider::class,

        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,

        Sebwite\Illuminate\Config\ConfigServiceProvider::class,

        Sebwite\Support\SupportServiceProvider::class,


        #Illuminate\Hashing\HashServiceProvider::class,
        #Illuminate\Mail\MailServiceProvider::class,
        #Illuminate\Pagination\PaginationServiceProvider::class,
        #Illuminate\Queue\QueueServiceProvider::class,
        #Illuminate\Redis\RedisServiceProvider::class,
        #Illuminate\Translation\TranslationServiceProvider::class,
        #Illuminate\Validation\ValidationServiceProvider::class,
        #Illuminate\Broadcasting\BroadcastServiceProvider::class,
        #Illuminate\Bus\BusServiceProvider::class,
        #Illuminate\Routing\ControllerServiceProvider::class,
        #Illuminate\Cookie\CookieServiceProvider::class,
        #Illuminate\Encryption\EncryptionServiceProvider::class,
        #Radic\BladeExtensions\BladeExtensionsServiceProvider::class,
    ],
    'providers-dev' => [
        Sebwite\Phpstorm\PhpstormServiceProvider::class,
        Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class
    ],
    'aliases'   => [
        'Schema' => Illuminate\Support\Facades\Schema::class
    ],

];
