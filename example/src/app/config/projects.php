<?php

/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
return [
    'config_file' => '.sebwite.php',
    'paths' => []
];
