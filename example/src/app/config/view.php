<?php

return [
    'paths' => [
        realpath(export_path('resources/views')),
    ],
    'compiled' => realpath(app()->storagePath() . '/framework/views' ),

];
