<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Illuminate\Foundation\Console;

/**
 * This is the class Command.
 *
 * @package        Sebwite\Illuminate
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 *
 * @property \Sebwite\Illuminate\Foundation\Application $laravel
 * @method \Sebwite\Illuminate\Foundation\Application getLaravel()
 */
abstract class Command extends \Sebwite\Support\Console\Command
{

}