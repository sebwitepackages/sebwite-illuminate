<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Illuminate\Foundation\Providers;


use Illuminate\Database\SeedServiceProvider as ServiceProvider;

class SeedServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('seeder', function ()
        {
            $class = '\Illuminate\Database\Seeder';
            return new $class;
        });

        if ( config('app.debug') )
        {
            $this->registerSeedCommand();

            $this->commands('command.seed');
        }
    }

}
