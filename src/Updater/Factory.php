<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Illuminate\Updater;


use Herrera\Phar\Update\Manager;
use Herrera\Phar\Update\Manifest;
use Illuminate\Contracts\Foundation\Application;

class Factory
{
    /**
     * @var \Illuminate\Contracts\Foundation\Application
     */
    protected $app;

    /**
     * @var bool
     */
    protected $enabled = true;

    /**
     * @var \Closure
     */
    protected $callback;

    /**
     * @var string
     */
    protected $manifestFile;

    /**
     * Factory constructor.
     *
     * @param $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }


    /**
     * @return mixed
     */
    public function getManifestFile()
    {
        return $this->manifestFile;
    }

    /**
     * Set the updateManifestFile value
     *
     * @param mixed $manifestFile
     *
     * @return Application
     */
    public function setManifestFile($manifestFile)
    {
        $this->manifestFile = $manifestFile;

        return $this;
    }

    public function getManifest()
    {
        return json_decode(file_get_contents($this->getManifestFile()), true);
    }

    /**
     * Set the updater value
     *
     * @param mixed $callback
     *
     * @return Application
     */
    public function setCallback($callback)
    {
        $this->callback = $callback;

        return $this;
    }

    /** @return \Closure */
    protected function getDefaultUpdater()
    {
        return function (Application $app)
        {
            $manager = new Manager(Manifest::loadFile($this->getManifestFile()));
            $manager->update($this->app->version(), true);
        };
    }

    public function update()
    {
        $updater = isset($this->callback) ? $this->callback : $this->getDefaultUpdater();

        return $this->app->call($updater);
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set the updateEnabled value
     *
     * @param boolean $enabled
     *
     * @return Application
     */
    public function enable()
    {
        $this->enabled = true;

        return $this;
    }

    public function disable()
    {
        $this->enabled = false;

        return $this;
    }
}