<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Illuminate\Updater;


use Sebwite\Support\ServiceProvider;

class UpdaterServiceProvider extends ServiceProvider
{
    protected $commands = [
        UpdateCommand::class
    ];

    protected $singletons = [
        'updater' => Factory::class
    ];
}